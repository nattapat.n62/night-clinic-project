import React, { useState } from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { Button, Grid, TextField, Typography } from '@mui/material';

export default function SimpleContainer() {
  const handleSubmit = event => {

  }
  
  const [type, setType] = useState('');
  const [disease, setDisease] = useState('');
  const [oname, setOname] = useState('');
  const [datein, setDatein] = useState('');
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" sx={{ p: 2 }}>
        <Typography variant="h6" gutterBottom component="div">
          Add Information
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField id="type" label="Type" variant="outlined"
                fullWidth required 
                onChange={(e) => setType(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField id="disease" label="Disease" variant="outlined"
                fullWidth required 
                onChange={(e) => setDisease(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField id="oname" label="Owner Name" variant="outlined"
                fullWidth required 
                onChange={(e) => setOname(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField id="datein" label="Date In" variant="outlined"
                fullWidth required 
                onChange={(e) => setDatein(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <Button variant="contained" fullWidth>Add</Button>
            </Grid>
          </Grid>
        </form>
      </Container>
    </React.Fragment>
  );
}
