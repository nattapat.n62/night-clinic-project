import Navbar from './Navbar';
import Clinic from './Clinics';


function App() {
  return (
    <div>
      <Navbar />
      <Clinic />
    </div>
  );
}

export default App;